#include <iostream>
#include <time.h>
#include <cstdlib>
#include <string>
#include <conio.h>

using namespace std;

class Karta {
private:
    int kolor;
    int numer;

public:
    void tworzJednaKarte(int k, int nr) {
        this->kolor = k;
        this->numer = nr;
    }

    Karta tworzKarte(int k, int nr) {
        Karta kartonik;
        kartonik.kolor = k;
        kartonik.numer = nr;
        return kartonik;
    }
    void wyswietlKarte() {
        string kolory[] = { "\x6", "\x4", "\x3", "\x5" };
        string numery[] = { "  ", " A", " 2", " 3", " 4", " 5", " 6", " 7", " 8", " 9", "10", " W", " D", " K" };
        if (this->numer == 0) {

            cout << " " << numery[(this->numer)] << "   ";
        }
        else {
            cout << "|" << numery[(this->numer)] << kolory[(this->kolor)] << " |";

        }
    }
    int getNumer() {
        return this->numer;
    }
};


class Talia : public Karta {
public:
    Karta talia[52];
    bool uzyta[52];


    void tasuj(Karta *tab) {
        int i, l;
        Karta pom;
        srand(time(NULL));
        for (i = 0; i < 52; i++) {
            l = (rand() % (52 - i)) + i;
            if (l != i) {
                pom = tab[i];
                tab[i] = tab[l];
                tab[l] = pom;
            }
        }
    }
    void tworzTalie() {
        for (int i = 0; i < 52; i++) {
            talia[i] = tworzKarte(i % 4, i % 13 + 1);

        }
        tasuj(talia);
    }

    void wyswietlTalie() {
        for (int i = 0; i < 52; i++) {
            talia[i].wyswietlKarte();
        }
    }

    Karta wybiezKatre(int nr) {
        return talia[nr];
    }
};


class Plansza : public Talia {

private:
    Karta piramidaGry[8][8];
    Karta stosGry[52];
    Karta taliaGry[52];
    Karta pole1 = tworzKarte(0, 0);
    Karta pole2 = tworzKarte(0, 0);
public:
    void zerowanie() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                this->piramidaGry[i][j] = tworzKarte(0, -1);
            }
        }

        for (int i = 0; i < 52; i++) {
            this->taliaGry[i] = tworzKarte(0, 0);
            this->stosGry[i] = tworzKarte(0, 0);
        }

    }

    void losujPiramide() {
        Talia t;
        t.tworzTalie();
        int counter = 0;

        zerowanie();

        for (int i = 0; i < 7; i++) {
            for (int j = 0; j <= i; j++) {
                this->piramidaGry[i][j] = t.wybiezKatre(counter);
                counter++;

            }

        }

        for (int i = 0; i < 8; i++) {

            this->piramidaGry[7][i] = tworzKarte(0, 0);
        }

        for (int i = counter; i < 52; i++) {
            this->taliaGry[i - counter] = t.wybiezKatre(i);

        }
    }


    Karta zwrocZPiramidy(int wybor) {
        wybor -= 1;
        for (int i = 0; i < 7; i++) {
            if (wybor <= i + 1) {
                if (this->piramidaGry[i + 1][wybor].getNumer() == 0 && this->piramidaGry[i + 1][wybor + 1].getNumer() == 0) return this->piramidaGry[i][wybor];
            }
        }
        return tworzKarte(0, 0);

    }

    Karta zwrocZStosu() {
        return this->stosGry[0];
    }

    void usunZPiramidy(int wybor){
        wybor -= 1;
        for (int i = 0; i < 7; i++) {
            if (wybor <= i + 1) {
                if (this->piramidaGry[i + 1][wybor].getNumer() == 0 && this->piramidaGry[i + 1][wybor + 1].getNumer() == 0)  this->piramidaGry[i][wybor] = tworzKarte(0, 0);
            }
        }
    }

    void usunZStosu() {
        this->stosGry[0] = tworzKarte(0, 0);
        for (int i = 0; i < 51; i++) {
            this->stosGry[i] = this->stosGry[i + 1];
        }
    }

    bool sprawdzSume() {
        int suma = this->pole1.getNumer() + this->pole2.getNumer();
        if (suma == 13) {
            return true;
        }
        else {
            return false;
        }
    }

    void kopiaDoPola(Karta karta, int nr) {
        if (nr == 1) this->pole1 = karta;
        if (nr == 2) this->pole2 = karta;
    }

    void stosDoTalii() {
        for (int i = 0; i < 52; i++) {
            this->taliaGry[i] = this->stosGry[i];
            this->stosGry[i] = tworzKarte(0, 0);
        }
        int counter = 0;
        while (this->taliaGry[counter].getNumer() != 0) {
            counter++;
        }
        Karta temp;

        for (int i = 0; i<counter / 2; i++) {
            temp = this->taliaGry[counter - i - 1];
            this->taliaGry[counter - i - 1] = this->taliaGry[i];
            this->taliaGry[i] = temp;
        }

    }

    void zerujPola() {
        this->pole1= tworzKarte(0, 0);
        this->pole2 = tworzKarte(0, 0);
    }

    void przerzutNaStos() {
        Karta temp;
        if (this->taliaGry[0].getNumer() != 0) {
            for (int i = 50; i >= 0; i--) {
                this->stosGry[i + 1] = this->stosGry[i];
            }
            this->stosGry[0] = this->taliaGry[0];
            for (int i = 0; i <52; i++) {
                this->taliaGry[i] = this->taliaGry[i + 1];
            }

        }
        else {
            stosDoTalii();
        }
    }

    void display() {
        cout << endl << endl;
        for (int i = 0; i<7; i++) {
            for (int k = 0; k < (7 - i)*3.2; k++) {
                cout << " ";
            }
            for (int j = 0; j<i + 1; j++) {
                this->piramidaGry[i][j].wyswietlKarte();
                cout << " ";
            }
            cout << endl << endl;
        }
        cout << "Stos [Q]: ";
        this->stosGry[0].wyswietlKarte();
        cout << endl << endl;
        cout << "Dodaj karte [T]";
        cout << endl << endl;
        cout << "Wybrana karta: ";
        this->pole1.wyswietlKarte();
        cout << endl << endl;
    }

     bool zwyciestwo() {
        if (this->piramidaGry[0][0].getNumer() == 0)return false;
        else return true;
    }

};


int main() {
    system("COLOR 37");
    Plansza plac;
    plac.losujPiramide();

    char zPiramidy1=0;
    char zPiramidy2=0;
    bool zStosu = false;
    int licznik=1;
    char sterownik;
    do {
        plac.display();
        cin >> sterownik;
        switch (sterownik)
        {
        case '1': {
            if (plac.zwrocZPiramidy(1).getNumer() == 13) { plac.usunZPiramidy(1); break; }
            plac.kopiaDoPola(plac.zwrocZPiramidy(1),licznik);
            if (licznik == 1)zPiramidy1 = sterownik;
            if (licznik == 2)zPiramidy2 = sterownik;
            licznik++;
            if (licznik == 3) {
                licznik = 1;
                if (plac.sprawdzSume()) {

                    if (zPiramidy1 != 0)plac.usunZPiramidy(zPiramidy1 - '0');
                    if (zPiramidy2 != 0)plac.usunZPiramidy(zPiramidy2 - '0');
                    if (zStosu == true) plac.usunZStosu();
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
                else {
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
            }
            break;
        }

        case '2':
        {
            if (plac.zwrocZPiramidy(2).getNumer() == 13) { plac.usunZPiramidy(2); break; }
            plac.kopiaDoPola(plac.zwrocZPiramidy(2), licznik);
            if (licznik == 1)zPiramidy1 = sterownik;
            if (licznik == 2)zPiramidy2 = sterownik;
            licznik++;
            if (licznik == 3) {
                licznik = 1;
                if (plac.sprawdzSume()) {

                    if (zPiramidy1 != 0)plac.usunZPiramidy(zPiramidy1 - '0');
                    if (zPiramidy2 != 0)plac.usunZPiramidy(zPiramidy2 - '0');
                    if (zStosu == true) plac.usunZStosu();
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
                else {
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
            }
            break;
        }

        case '3':
        {
            if (plac.zwrocZPiramidy(3).getNumer() == 13) { plac.usunZPiramidy(3); break; }
            plac.kopiaDoPola(plac.zwrocZPiramidy(3), licznik);
            if (licznik == 1)zPiramidy1 = sterownik;
            if (licznik == 2)zPiramidy2 = sterownik;
            licznik++;
            if (licznik == 3) {
                licznik = 1;
                if (plac.sprawdzSume()) {

                    if (zPiramidy1 != 0)plac.usunZPiramidy(zPiramidy1 - '0');
                    if (zPiramidy2 != 0)plac.usunZPiramidy(zPiramidy2 - '0');
                    if (zStosu == true) plac.usunZStosu();
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
                else {
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
            }
            break;
        }

        case '4':
        {
            if (plac.zwrocZPiramidy(4).getNumer() == 13) { plac.usunZPiramidy(4); break; }
            plac.kopiaDoPola(plac.zwrocZPiramidy(4), licznik);
            if (licznik == 1)zPiramidy1 = sterownik;
            if (licznik == 2)zPiramidy2 = sterownik;
            licznik++;
            if (licznik == 3) {
                licznik = 1;
                if (plac.sprawdzSume()) {

                    if (zPiramidy1 != 0)plac.usunZPiramidy(zPiramidy1 - '0');
                    if (zPiramidy2 != 0)plac.usunZPiramidy(zPiramidy2 - '0');
                    if (zStosu == true) plac.usunZStosu();
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
                else {
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
            }
            break;
        }

        case '5':
        {
            if (plac.zwrocZPiramidy(5).getNumer() == 13) { plac.usunZPiramidy(5); break; }
            plac.kopiaDoPola(plac.zwrocZPiramidy(5), licznik);
            if (licznik == 1)zPiramidy1 = sterownik;
            if (licznik == 2)zPiramidy2 = sterownik;
            licznik++;
            if (licznik == 3) {
                licznik = 1;
                if (plac.sprawdzSume()) {

                    if (zPiramidy1 != 0)plac.usunZPiramidy(zPiramidy1 - '0');
                    if (zPiramidy2 != 0)plac.usunZPiramidy(zPiramidy2 - '0');
                    if (zStosu == true) plac.usunZStosu();
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
                else {
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
            }
            break;
        }

        case '6':
        {
            if (plac.zwrocZPiramidy(6).getNumer() == 13) { plac.usunZPiramidy(6); break; }
            plac.kopiaDoPola(plac.zwrocZPiramidy(6), licznik);
            if (licznik == 1)zPiramidy1 = sterownik;
            if (licznik == 2)zPiramidy2 = sterownik;
            licznik++;
            if (licznik == 3) {
                licznik = 1;
                if (plac.sprawdzSume()) {

                    if (zPiramidy1 != 0)plac.usunZPiramidy(zPiramidy1 - '0');
                    if (zPiramidy2 != 0)plac.usunZPiramidy(zPiramidy2 - '0');
                    if (zStosu == true) plac.usunZStosu();
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
                else {
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
            }
            break;
        }

        case '7':
        {
            if (plac.zwrocZPiramidy(7).getNumer() == 13) { plac.usunZPiramidy(7); break; }
            plac.kopiaDoPola(plac.zwrocZPiramidy(7), licznik);
            if (licznik == 1)zPiramidy1 = sterownik;
            if (licznik == 2)zPiramidy2 = sterownik;
            licznik++;
            if (licznik == 3) {
                licznik = 1;
                if (plac.sprawdzSume()) {

                    if (zPiramidy1 != 0)plac.usunZPiramidy(zPiramidy1 - '0');
                    if (zPiramidy2 != 0)plac.usunZPiramidy(zPiramidy2 - '0');
                    if (zStosu == true) plac.usunZStosu();
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
                else {
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
            }
            break;
        }

        case 'q':
        {
            if (plac.zwrocZStosu().getNumer() == 13) { plac.usunZStosu(); break; }
            plac.kopiaDoPola(plac.zwrocZStosu(), licznik);
            zStosu = true;
            licznik++;
            if (licznik == 3) {
                licznik = 1;
                if (plac.sprawdzSume()) {

                    if (zPiramidy1 != 0)plac.usunZPiramidy(zPiramidy1 - '0');
                    if (zPiramidy2 != 0)plac.usunZPiramidy(zPiramidy2 - '0');
                    if (zStosu == true) plac.usunZStosu();
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
                else {
                    plac.zerujPola();
                    zPiramidy1 = 0;
                    zPiramidy2 = 0;
                    zStosu = false;

                }
            }
            break;
        }

        case 't':
            plac.przerzutNaStos();
            break;

        default:
            cout << "Naciskaj tylko to co mozesz";
        }
        system("cls");
      } while (plac.zwyciestwo());
 cout << "GRATULACJE WYGRALES!!!";
 _getch();
 return 0;
}
